import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  container: {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    background: '#82ada9',
    transition: 'all 1s ease-in',
    overflow: 'hidden',
  },
  content: {
    flex: 1,
    display: 'flex',
    zIndex: 10,
    overflow: 'hidden',
  },
}));
