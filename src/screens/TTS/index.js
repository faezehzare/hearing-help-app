import { useEffect, useState } from 'react';

import { IconButton, TextField } from '@material-ui/core';
import { ClearRounded } from '@material-ui/icons';

import Card from '@/components/Card';
import RecButton from './PlayButton';
import { PrettoSlider } from './Slider';

import useStyles from './styles';

const synth = window.speechSynthesis;

if (!('speechSynthesis' in window))
  alert("Sorry, your browser doesn't support text to speech!");

export default function Dictaphone() {
  const cls = useStyles();

  const [text, setText] = useState('');
  const [isPlay, setIsPlay] = useState(false);
  const [rate, setRate] = useState(1);
  const [pitch, setPitch] = useState(1);

  useEffect(() => {
    const AslEl = document.querySelector('#asl-container');
    if (AslEl) AslEl.scrollTop = AslEl.scrollHeight;
  }, [text]);

  const handleStop = () => synth.cancel();

  const handleStart = () => {
    if (text === '') {
      alert('Please write some text.');
      return;
    }

    const utterThis = new SpeechSynthesisUtterance(text);
    utterThis.onend = (e) => setIsPlay(false);

    utterThis.onerror = (e) => {
      console.error('SpeechSynthesisUtterance.onerror', e);
      alert('Some thing went wrong!!');
    };

    // utterThis.voice = voices[i];

    utterThis.pitch = pitch;
    utterThis.rate = rate;

    setIsPlay(true);
    synth.speak(utterThis);
  };

  return (
    <div className={cls.content}>
      <TextField
        placeholder="Type here..."
        multiline
        rows={5}
        rowsMax={5}
        fullWidth
        value={text}
        onChange={(e) => setText(e.target.value)}
      />

      <Card className={cls.card2}>
        <div id="asl-container">{text}</div>
      </Card>

      <Card>
        <div className={cls.slider}>
          <span>Rate: </span>
          <PrettoSlider
            step={0.1}
            min={0.5}
            max={2}
            valueLabelDisplay="auto"
            value={rate}
            onChange={(e, v) => setRate(v)}
          />
        </div>
        <div className={cls.slider} style={{ marginTop: 10 }}>
          <span>Pitch: </span>
          <PrettoSlider
            step={0.1}
            max={2}
            valueLabelDisplay="auto"
            value={pitch}
            onChange={(e, v) => setPitch(v)}
          />
        </div>

        <div className={cls.row}>
          <div>
            <IconButton className={cls.clear} onClick={() => setText('')}>
              <ClearRounded />
            </IconButton>
            <h3>Clear</h3>
          </div>

          <div>
            <RecButton
              isPlay={isPlay}
              handleStop={handleStop}
              handleStart={handleStart}
            />
            <h3>{isPlay ? 'Stop' : 'Play'}</h3>
          </div>
        </div>
      </Card>
    </div>
  );
}
