import { makeStyles } from '@material-ui/core/styles';

export default makeStyles({
  content: {
    padding: '20px',
    flex: 1,
    overflowY: 'auto',
    overflowX: 'hidden',
    '& textarea': {
      color: '#fff',
    },
  },
  row: {
    marginTop: 20,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    '& > div': {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    '& h3': {
      color: '#52af77',
      margin: '15px 0 0',
      fontWeight: 'normal',
    },
  },

  command: {
    marginBottom: 30,
    '& div': {
      fontSize: 18,
    },
  },
  card: {
    height: 300,
    overflowY: 'auto',
    overflowX: 'hidden',
    '& span': {
      flex: 1,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      color: '#e69138',
      fontSize: 18,
    },
  },
  card2: {
    margin: '10px 0',
    padding: '5px 10px',

    '& div': {
      height: 180,
      overflowY: 'auto',
      overflowX: 'hidden',
      fontFamily: 'Gallaudet !important',
      fontSize: 65,
      lineHeight: '55px',
      wordBreak: 'break-all',
    },
  },
  clear: {
    boxShadow: '0px 0px 8px rgb(0 0 0 / 16%)',
    backgroundColor: '#EAEAEa',
    border: 'solid #351c75 3px',
    '& svg': {
      fontSize: 35,
    },
    '&:hover': {
      backgroundColor: '#EAEAEa !important',
    },
  },
  slider: {
    display: 'flex',
    alignItems: 'center',
    '& > span': {
      color: '#52af77',
      margin: '0 10px 0 0',
    },
  },
});
