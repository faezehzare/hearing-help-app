import { useEffect } from 'react';
import SpeechRecognition, {
  useSpeechRecognition,
} from 'react-speech-recognition';
import { IconButton } from '@material-ui/core';
import { ClearRounded } from '@material-ui/icons';

import Card from '@/components/Card';
import RecButton from './RecButton';

import useStyles from './styles';

if (!SpeechRecognition.browserSupportsSpeechRecognition()) {
  alert('not Support on your device');
}

export default function Dictaphone() {
  const cls = useStyles();

  const commands = [
    {
      command: 'clear',
      callback: ({ resetTranscript }) => resetTranscript(),
    },
    {
      command: 'Exit',
      callback: () => {
        if (window.confirm('Close Window?')) {
          window.close();
        }
      },
    },
    {
      command: 'Stop',
      callback: () => handleStop(),
    },
  ];

  const { transcript, resetTranscript, listening } = useSpeechRecognition({
    commands,
  });

  useEffect(() => {
    const AslEl = document.querySelector('#asl-container');
    if (AslEl) AslEl.scrollTop = AslEl.scrollHeight;
  }, [transcript]);

  const handleStart = () =>
    SpeechRecognition.startListening({ continuous: true });

  const handleStop = () => SpeechRecognition.stopListening();

  return (
    <div className={cls.content}>
      <Card className={cls.card}>
        {transcript ? (
          transcript
        ) : (
          <span>Click the start button, Say some words!</span>
        )}
      </Card>
      <Card className={cls.card2}>
        <div id="asl-container">{transcript}</div>
      </Card>

      <div className={cls.row}>
        <div>
          <IconButton className={cls.clear} onClick={() => resetTranscript()}>
            <ClearRounded />
          </IconButton>
          <h3>Clear</h3>
        </div>

        <div>
          <RecButton
            isRecord={listening}
            handleStop={handleStop}
            handleStart={handleStart}
          />
          <h3>{listening ? 'Stop' : 'Start'}</h3>
        </div>
      </div>
    </div>
  );
}
