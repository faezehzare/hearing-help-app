import { makeStyles } from '@material-ui/core/styles';

export default makeStyles({
  content: {
    padding: '20px',
    flex: 1,
    overflowY: 'auto',
    overflowX: 'hidden',
  },
  row: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    '& > div': {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    '& h3': {
      color: '#fff',
      margin: '10px 0 0 0',
      fontWeight: 'normal',
    },
  },

  command: {
    marginBottom: 30,
    '& div': {
      fontSize: 18,
    },
  },
  card: {
    height: 250,
    overflowY: 'auto',
    overflowX: 'hidden',
    '& span': {
      flex: 1,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      fontSize: 18,
      textAlign: 'center',
    },
  },
  card2: {
    padding: '5px 10px',
    margin: '10px 0 30px',
    '& div': {
      height: 120,
      overflowY: 'auto',
      overflowX: 'hidden',
      fontFamily: 'Gallaudet !important',
      fontSize: 65,
      lineHeight: '55px',
      wordBreak: 'break-all',
    },
  },
  clear: {
    boxShadow: '0px 0px 8px rgb(0 0 0 / 16%)',
    backgroundColor: '#EAEAEa',
    border: 'solid #351c75 3px',
    '& svg': {
      fontSize: 35,
    },
    '&:hover': {
      backgroundColor: '#EAEAEa !important',
    },
  },
});
