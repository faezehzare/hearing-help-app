import { IconButton } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { MicRounded, MicOffRounded } from '@material-ui/icons';

const useStyles = makeStyles({
  btn: {
    '& button': {
      boxShadow: '0px 0px 8px rgb(0 0 0 / 16%)',
      backgroundColor: '#EAEAEa',
      '& svg': {
        fontSize: 35,
      },
      '&:hover': {
        backgroundColor: '#EAEAEa !important',
      },
    },
  },
  rec: {
    position: 'relative',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    '& svg': {
      color: '#36B37E',
    },
    '& button': {
      border: 'solid #36B37E 3px',
    },
  },

  '@keyframes ripple': {
    '0%': {
      transform: 'scale(1)',
    },
    '50%': {
      transform: 'scale(2)',
      opacity: 0.3,
    },
    '100%': {
      transform: 'scale(1)',
    },
  },
  '@keyframes ripple-2': {
    '0%': {
      transform: 'scale(1)',
    },
    '50%': {
      transform: 'scale(2.5)',
      opacity: 0.3,
    },
    '100%': {
      transform: 'scale(1)',
    },
  },

  circle1: {
    position: 'absolute',
    height: 60,
    width: 60,
    backgroundColor: '#36B37E',
    borderRadius: '50%',
    animation: '$ripple 2s infinite',
  },
  circle2: {
    position: 'absolute',
    height: 60,
    width: 60,
    backgroundColor: '#36B37E',
    borderRadius: '50%',
    animation: '$ripple-2 2s infinite',
    zIndex: 0,
  },

  stop: {
    border: 'solid #c4001d 3px',
    '& svg': {
      color: '#c4001d',
    },
  },
});

export default function RecButton({ isRecord, handleStart, handleStop }) {
  const cls = useStyles();
  return (
    <div className={cls.btn}>
      {isRecord ? (
        <div className={cls.rec}>
          <div className={cls.circle1} />
          <div className={cls.circle2} />
          <IconButton onClick={handleStop}>
            <MicRounded />
          </IconButton>
        </div>
      ) : (
        <IconButton className={cls.stop} onClick={handleStart}>
          <MicOffRounded />
        </IconButton>
      )}
    </div>
  );
}
