import { makeStyles } from '@material-ui/core/styles';

export default makeStyles({
  content: {
    padding: '20px',
    flex: 1,
    overflowY: 'auto',
    overflowX: 'hidden',
  },
  alpha: {
    '& > div': {
      fontSize: 20,
      fontWeight: 'bold',
      textTransform: 'capitalize',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      color: '#0065a3',
    },
    '& span': {
      fontWeight: 'normal',
      fontFamily: 'Gallaudet !important',
      fontSize: 90,
      lineHeight: '70px',
      color: '#000',
    },
  },
});
