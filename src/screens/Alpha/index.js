import { Grid } from '@material-ui/core';
import Card from '@/components/Card';

import useStyles from './styles';

const Alphabet = 'abcdefghijklmnopqrstuvwxyz'.split('');

export default function Alphabets() {
  const cls = useStyles();
  return (
    <div className={cls.content}>
      <Card className={cls.card}>
        <Grid container spacing={2} className={cls.alpha}>
          {Alphabet.map((i, idx) => (
            <Grid item xs={6} key={idx}>
              <span>{i}</span>
              {i}
            </Grid>
          ))}
        </Grid>
      </Card>
    </div>
  );
}
