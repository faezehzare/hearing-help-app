import { useState } from 'react';
import BottomTab from '@/components/BottomTab';

import Alpha from '@/screens/Alpha';
import Speech from '@/screens/Speech';
import TTS from '@/screens/TTS';

import styles from './styles';

export default ({ history }) => {
  const cls = styles();
  const [tab, setTab] = useState('speech');

  const Comp = () => {
    switch (tab) {
      case 'speech':
        return <Speech />;
      case 'tts':
        return <TTS />;
      case 'alpha':
        return <Alpha />;
      default:
        return null;
    }
  };

  return (
    <div className={cls.container}>
      <div className={cls.content}>
        <Comp />
      </div>
      <BottomTab tab={tab} setActive={setTab} />
    </div>
  );
};
