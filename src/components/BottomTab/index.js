import Tab from './Tab';
import styles from './styles';

export const TabIcons = {
  mic: require('@/assets/icons/mic.svg').default,
  sound: require('@/assets/icons/sound.svg').default,
  alpha: require('@/assets/icons/abc.svg').default,
};

export default ({ tab, setActive }) => {
  const cls = styles();
  return (
    <div className={cls.bottomTab}>
      <Tab
        label="Speech to Text"
        onClick={() => setActive('speech')}
        icon={TabIcons.mic}
        isActive={tab === 'speech'}
      />
      <Tab
        label="Text to Speech"
        onClick={() => setActive('tts')}
        icon={TabIcons.sound}
        isActive={tab === 'tts'}
      />
      <Tab
        label="Alphabet"
        onClick={() => setActive('alpha')}
        icon={TabIcons.alpha}
        isActive={tab === 'alpha'}
      />
    </div>
  );
};
