import { useSpring, animated } from 'react-spring';
import { ReactSVG } from 'react-svg';
import styles from './styles';

export default ({ icon, onClick, label, isActive }) => {
  const cls = styles();
  const { xy, clipPath1 } = useSpring({
    clipPath1: isActive
      ? 'polygon(0 0, 0% 0, 0% 0%, 0% 0%)'
      : 'polygon(0 0, 100% 0, 100% 100%, 0% 100%)',

    xy: !isActive ? [0, 10] : [0, -10],
  });

  const { xy2, clipPath2, size } = useSpring({
    xy2: !isActive ? [0, 0] : [0, -15],
    clipPath2: isActive
      ? 'polygon(0 0, 100% 0, 100% 100%, 0% 100%)'
      : 'polygon(0 0, 0% 0, 0% 0%, 0% 0%)',
    size: isActive ? 1 : 0,
  });

  return (
    <div className={cls.tab} onClick={onClick}>
      <animated.div
        style={{
          transform: xy.interpolate((x, y) => `translate(${x}px, ${y}px)`),
        }}>
        <animated.div style={{ clipPath: clipPath1 }}>
          <ReactSVG src={icon} className={cls.icon} />
        </animated.div>
      </animated.div>
      <animated.div
        style={{
          transform: xy2.interpolate((x, y) => `translate(${x}px, ${y}px)`),
        }}>
        <animated.div style={{ clipPath: clipPath2 }} className={cls.label}>
          {label}
        </animated.div>
      </animated.div>
      <animated.div
        className={cls.item}
        style={{
          transform: size.interpolate(
            (size) => `translateY(-10px) scale(${size})`
          ),
        }}
      />
    </div>
  );
};
