import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  bottomTab: {
    animation: 'backInUp 1s',
    display: 'flex',
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: '#fff',
    borderRadius: '20px 20px 0 0',
    padding: '0 15px',
    boxShadow: '0px 0px 10px 0px #18236b33',
    margin: '10px -1% 0',
  },
  tab: {
    padding: 10,
    cursor: 'pointer',
  },
  item: {
    height: 5,
    width: 5,
    borderRadius: '50%',
    backgroundColor: '#050950',
    margin: 'auto',
  },
  label: {
    fontSize: 12,
    fontWeight: 'bold',
    color: '#050950',
    display: 'flex',
    justifyContent: 'center',
  },
  icon: {
    width: 30,
    height: 30,
  },
}));
