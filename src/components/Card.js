import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

const useStyles = makeStyles({
  card: {
    display: 'flex',
    flexDirection: 'column',
    borderRadius: 10,
    boxShadow: '0px 8px 8px rgba(0, 0, 0, 0.12)',
    backgroundColor: '#eefffe',
    padding: '15px 10px',
    transition: 'all 0.3s ease-in-out',
  },
});

export default function Card({ children, className }) {
  const cls = useStyles();
  return <div className={clsx(cls.card, className)}>{children}</div>;
}
