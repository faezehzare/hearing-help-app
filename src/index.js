import ReactDOM from 'react-dom';
import './assets/global.css';
import App from './App';

ReactDOM.render(<App />, document.getElementById('root'));
